package com.example

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
class TestController() {

    @Value("\${server.email:}")
    val stringProfile: String? = null

    @GetMapping("/verify", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getVerifyProfile() = stringProfile
}
